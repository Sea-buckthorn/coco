init("0","1")
width,height = getScreenSize()
ocr, msg = createOCR({
		type = "tesseract",
		path = "res/", -- 自定义字库暂时只能放在脚本res/目录下
		lang = "num" -- 使用生成的num.traineddata文件
	})

if ocr ~= nil then
	-- ocr 创建成功，使用该实例进行后续识别操作（参见下面函数文档）
	sysLog("createOCR succeed: Tesseract-OCR v" .. msg)
else
	-- ocr 创建失败，根据msg提示调整
	sysLog("createOCR failed: " .. tostring(msg))
end
function waitForUI()
	mSleep(2000)
end
function isShowCoCo()
	x, y = findColor({33, 134, 240, 244},
		"0|0|0xfffbcc",
		95, 0, 0, 1)
	if x > -1 then
		x, y = findColor({33, 134, 240, 244},
			"0|0|0xffe8fd",
			95, 0, 0, 1)
		if x > -1 then
			return false
		else
			return true
		end
		
	else
		
		
		return true
	end
end
function startAttack()
	
	x, y = findColor({46, 874, 202, 1026},
		"0|0|0xbd5a1f,-9|-64|0xefa437",
		95, 0, 0, 1)
	if x > -1 then
		click(x,y)
		waitForUI()
		return true
	else
		dialog("不在主界面",0)
		return false
	end
	
end
function startSearch()
	x, y = findColor({238, 686, 614, 843},
		"0|0|0xffa936,203|61|0xe75d0e",
		95, 0, 0, 1)
	if x > -1 then
		click(x,y)
		waitForUI()
		return true
	else
		dialog("不在搜索界面",0)
		return false
	end
	
end



function click(x,y)
	touchDown(1, x, y);            --那么单击该图片
	touchUp(1, x, y);
end

function nextResouce()
	width,height = getScreenSize()
	x, y = findImageInRegionFuzzy("next.png", 50, 0, 0, height, width, 0xffffff)
	if x ~= -1 and y ~= -1 then        --如果在指定区域找到某图片符合条件
		click(x,y)
	else                               --如果找不到符合条件的图片
		dialog("未找到符合条件的坐标！",0);
	end
end



string.trim = function(s)
	return string.gsub(s," ","")
end

function isenmpty(s)
	return s:match("^%s")
end
function getJinbi()
	local rect = {99,138,250,168}
	local diff = {"0xfffbcc-0x000000"}
	local result=""
	local code, text = ocr:getText({
			rect = rect,
			diff = diff
		})
	if code == 0 then
		local shuiStr=text:trim()
		sysLog(shuiStr)
		result=shuiStr
	else
		sysLog("recognize failed: code = " .. tostring(code))
	end
	return result
end

function getShui()
	
	--shui
	local rect = {99,195,250,224}
	local diff = {"0xffe8fd-0x000000"}
	local result=""
	local code, text = ocr:getText({
			rect = rect,
			diff = diff
		})
	if code == 0 then
		local shuiStr=text:trim()
		sysLog(shuiStr)
		result=shuiStr
		
		
	else
		sysLog("recognize failed: code = " .. tostring(code))
	end
	return result
end


function getZiyuan()
	while(isShowCoCo())
	do
		sysLog("资源未显示，休眠1000后继续搜索")
		mSleep(1000)
	end
	mSleep(2000)
	local result={}
	
	
	
	local jinbi=getJinbi()
	local findJinbi=false
	while(findJinbi==false)
	do
		if(jinbi=='')then
			jinbi=getJinbi()
		else
			result.jinbi=tonumber(jinbi)
			findJinbi=true
			break
		end
		
	end
	local shui= getShui()
	local findShui=false
	
	while(findShui==false)
	do
		if(shui=='')then
			shui=getShui()
		else
			result.shui=tonumber(shui)
			findShui=true
			break
		end
		
	end
	
	return result
	
end

function getNextCoCo()
	click(1750,779)
end

function aotoClick()
	mSleep(30000)
	while(true)
	do
		mSleep(30000)
		while (true)
		do
			x, y = findColor({506, 35, 1574, 809},
				"0|0|0xea3dd7",
				95, 0, 0, 1)
			if x > -1 then
				click(x,y)
			else
				break
			end
			
		end
		while (true)
		do
			x, y = findColor({506, 35, 1574, 809},
				"0|0|0xffec48,1|17|0x9d5800",
				95, 0, 0, 1)
			if x > -1 then
				
			end
			if x > -1 then
				click(x,y)
			else
				break
			end
			
		end
		
		sysLog("点击+1====")
	end
end